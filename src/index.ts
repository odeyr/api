import { GraphQLServer } from 'graphql-yoga'
import { prisma } from './generated/prisma-client'
import * as cookieParser from 'cookie-parser'
import resolvers from './resolvers'
import isAuthenticated from './directives/isAuthenticated'
import isConfirmed from './directives/isConfirmed'
import hasScopes from './directives/hasScopes'
import { Prisma } from './generated/prisma'
import * as i18n from 'i18n'

i18n.configure({
  locales: ['en', 'fr'],
  directory: './locales',
  cookie: 'odeyr-lang',
  globalize: true,
  objectNotation: true,
  autoReload: true
})

const server = new GraphQLServer({
  typeDefs: './src/schema.graphql',
  resolvers,
  context: request => ({
    ...request,
    prisma,
    db: new Prisma({
      endpoint: process.env.PRISMA_ENDPOINT,
      secret: process.env.PRISMA_SECRET,
      debug: process.env.NODE_ENV === 'development'
    })
  }),
  schemaDirectives: {
    isAuthenticated,
    isConfirmed,
    hasScopes
  },
  resolverValidationOptions: {
    requireResolversForResolveType: false
  }
})

server.express.use(cookieParser(process.env.APP_SECRET || 'secret'))

server.express.use(i18n.init)

server.start(
  {
    port: process.env.PORT || 4000,
    cors: {
      origin: [
        'http://localhost:8080',
        'http://localhost:4000',
        'https://develop.api.odeyr.org',
        'https://api.odeyr.org',
        'https://dashboard.odeyr.org',
        'https://app.odeyr.org',
        'https://develop.odeyr.org',
        'https://odeyr.org'
      ],
      credentials: true
    }
  },
  opts => console.log(`Server is running on http://localhost:${opts.port}`)
)
