export const userWithScopesFragment = `
fragment UserWithScopes on User {
  id
  username
  email
  password
  refreshTokenVersion
  permissions {
    actionAllowed {
      name
    }
  }
  confirmationToken {
    id
    token
    confirmed
  }
}
`

export interface Permission {
  actionAllowed: { name: string }
}

export interface UserWithScopes {
  id: string
  username: string
  email: string
  password: string
  refreshTokenVersion: number
  permissions: Permission[]
  confirmationToken: {
    id: string
    token: string
    confirmed: boolean
  }
}

export const confirmationWithUserAndScopesFragment = `
fragment ConfirmationTokenWithUserAndScopes on EmailConfirmationToken {
  user {
    id
    username
    email
    password
    refreshTokenVersion
    permissions {
      actionAllowed {
        name
      }
    }
    confirmationToken {
      id
      token
      confirmed
    }
  }
}
`
export interface ConfirmationTokenWithUserAndScopes {
  user: UserWithScopes
}
