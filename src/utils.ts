import * as jwt from 'jsonwebtoken'
import { Prisma } from './generated/prisma-client'
import { Prisma as BasePrisma } from './generated/prisma'
import { ApolloError, AuthenticationError } from 'apollo-server'
import { Permission, userWithScopesFragment, UserWithScopes } from './fragments'
import * as ms from 'ms'
import * as token from 'crypto-token'
import mailService from './services/mailService'

export const REFRESH_TOKEN_TIME = process.env.REFRESH_TOKEN_TIME || '0.5y'
export const ACCESS_TOKEN_TIME = process.env.ACCESS_TOKEN_TIME || '15min'
export const CONFIRMATION_TOKEN_TIME =
  process.env.CONFIRMATION_TOKEN_TIME || '3d'
export const FORGOT_PASSWORD_TOKEN_TIME =
  process.env.FORGOT_PASSWORD_TOKEN_TIME || '1d'

export interface Context {
  prisma: Prisma
  db: BasePrisma
  request: any
  response: any
  auth: ContextAuth
}

interface ContextAuth {
  userId: string
  confirmed: boolean
  scopes: string[]
}

export const signUser = (
  userId: string,
  refreshTokenVersion: number,
  confirmed: boolean,
  scopes: string[]
): {
  refreshToken: string
  accessToken: string
} => {
  return {
    refreshToken: jwt.sign(
      { userId, version: refreshTokenVersion },
      process.env.REFRESH_SECRET,
      { expiresIn: REFRESH_TOKEN_TIME }
    ),
    accessToken: jwt.sign(
      { userId, confirmed, scopes },
      process.env.APP_SECRET,
      { expiresIn: ACCESS_TOKEN_TIME }
    )
  }
}

export function setTokensCookies(
  signedTokens: {
    refreshToken: string
    accessToken: string
  },
  ctx: Context
) {
  ctx.response.cookie('odeyr-refresh-token', signedTokens.refreshToken, {
    expires: new Date(Date.now() + ms(REFRESH_TOKEN_TIME)),
    httpOnly: true,
    secure: process.env.NODE_ENV === 'production' || false
  })
  ctx.response.cookie('odeyr-access-token', signedTokens.accessToken, {
    expires: new Date(Date.now() + ms(ACCESS_TOKEN_TIME)),
    httpOnly: true,
    secure: process.env.NODE_ENV === 'production' || false
  })
}

export const getScopes = (permissions: Permission[]): string[] => {
  return permissions.map(action => action.actionAllowed.name) as string[]
}

export async function authenticateUser(ctx: Context): Promise<ContextAuth> {
  const accessToken = ctx.request.cookies['odeyr-access-token']

  // Validate access token
  try {
    const { userId, confirmed, scopes } = jwt.verify(
      accessToken,
      process.env.APP_SECRET
    ) as ContextAuth
    if (!userId || !scopes)
      throw new ApolloError('Invalid Access Token!', 'ACCESS_TOKEN_INVALID')

    return {
      userId,
      confirmed,
      scopes
    }
    // eslint-disable-next-line prettier/prettier
  } catch { }

  // The access token has expired, let's check the refresh token
  const refreshToken = ctx.request.cookies['odeyr-refresh-token']

  try {
    // Validate the refresh token
    const { userId, version } = jwt.verify(
      refreshToken,
      process.env.REFRESH_SECRET
    ) as {
      userId: string
      version: number
    }
    if (!userId && !version && version !== 0)
      throw new ApolloError('Refresh Token Invalid!', 'REFRESH_TOKEN_INVALID')

    const user = (await ctx.prisma
      .user({ id: userId })
      .$fragment(userWithScopesFragment)) as UserWithScopes
    if (!user)
      throw new ApolloError('Refresh Token Invalid!', 'REFRESH_TOKEN_INVALID')

    // Now let's refresh token version
    if (user.refreshTokenVersion !== version) {
      throw new ApolloError('Refresh Token Expired!', 'REFRESH_TOKEN_EXPIRED')
    }

    const scopes = getScopes(user.permissions)
    const signedTokens = signUser(
      user.id,
      user.refreshTokenVersion,
      user.confirmationToken.confirmed,
      scopes
    )

    setTokensCookies(signedTokens, ctx)

    return {
      userId: user.id,
      confirmed: user.confirmationToken.confirmed,
      scopes
    }
  } catch {
    throw new AuthenticationError('You are not authenticated!')
  }
}

export function validateScopes(ctx: Context, requiredScopes): string[] {
  const userScopes = ctx.auth.scopes

  const mandatoryScopes = requiredScopes
    .split(',')
    .map(s => s.trim().toLowerCase())
  const scopesIntersection = userScopes.filter(role =>
    mandatoryScopes.includes(role.trim().toLowerCase())
  )

  if (scopesIntersection.length !== mandatoryScopes.length) {
    throw new ApolloError('Not Authorized', 'UNAUTHORIZED')
  }

  return scopesIntersection
}

export function sendConfirmationEmail(
  user: UserWithScopes,
  ctx: Context
): Promise<UserWithScopes> {
  return new Promise((resolve, reject) => {
    const confirmationToken = token(32)

    ctx.db.mutation
      .updateEmailConfirmationToken({
        data: {
          token: confirmationToken,
          confirmed: process.env.SKIP_ACCOUNT_CONFIRMATION === 'true' || false,
          expiresAt: new Date(Date.now() + ms(CONFIRMATION_TOKEN_TIME))
        },
        where: { id: user.confirmationToken.id }
      })
      .then(confirmationToken => {
        user.confirmationToken = confirmationToken

        const signedTokens = signUser(
          user.id,
          user.refreshTokenVersion,
          user.confirmationToken.confirmed,
          getScopes(user.permissions)
        )
        setTokensCookies(signedTokens, ctx)

        if (process.env.SKIP_ACCOUNT_CONFIRMATION === 'true')
          return resolve(user)

        mailService
          .send(
            user.email,
            ctx.request.__('email.account_confirmation_subject'),
            'account_confirmation',
            ctx.request.locale,
            {
              activationLink: `${process.env.VERIFICATION_DOMAIN}/${ctx.request.locale}/verify/${confirmationToken.token}`
            }
          )
          .then(() => {
            user.confirmationToken = confirmationToken
            return resolve(user)
          })
          .catch(error => reject(error))
      })
      .catch(error => reject(error))
  })
}
