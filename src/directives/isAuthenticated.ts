import { SchemaDirectiveVisitor } from 'graphql-tools'
import {
  DirectiveLocation,
  GraphQLDirective,
  defaultFieldResolver
} from 'graphql'
import { Context, authenticateUser } from '../utils'

export default class extends SchemaDirectiveVisitor {
  static getDirectiveDeclaration(directiveName = 'isAuthenticated') {
    return new GraphQLDirective({
      name: directiveName,
      locations: [DirectiveLocation.FIELD_DEFINITION]
    })
  }

  visitFieldDefinition(field: any) {
    const { resolve = defaultFieldResolver } = field

    field.resolve = async (
      root: any,
      args: any,
      context: Context,
      info: any
    ) => {
      const auth = await authenticateUser(context)

      return resolve.call(this, root, args, { ...context, auth }, info)
    }
  }
}
