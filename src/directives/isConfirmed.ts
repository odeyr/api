import { SchemaDirectiveVisitor } from 'graphql-tools'
import {
  DirectiveLocation,
  GraphQLDirective,
  defaultFieldResolver
} from 'graphql'
import { Context, authenticateUser } from '../utils'
import { ApolloError } from 'apollo-server'

export default class extends SchemaDirectiveVisitor {
  static getDirectiveDeclaration(directiveName = 'isConfirmed') {
    return new GraphQLDirective({
      name: directiveName,
      locations: [DirectiveLocation.FIELD_DEFINITION]
    })
  }

  visitFieldDefinition(field: any) {
    const { resolve = defaultFieldResolver } = field

    field.resolve = async (
      root: any,
      args: any,
      context: Context,
      info: any
    ) => {
      const auth = await authenticateUser(context)

      if (!auth.confirmed)
        throw new ApolloError(
          'Your email is not yet confirmed!',
          'EMAIL_NOT_CONFIRMED'
        )

      return resolve.call(this, root, args, context, info)
    }
  }
}
