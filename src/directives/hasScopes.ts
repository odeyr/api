import { SchemaDirectiveVisitor } from 'graphql-tools'
import {
  DirectiveLocation,
  GraphQLDirective,
  defaultFieldResolver,
  GraphQLString
} from 'graphql'
import { validateScopes, authenticateUser, Context } from '../utils'

export default class extends SchemaDirectiveVisitor {
  static getDirectiveDeclaration(directiveName = 'hasScopes') {
    return new GraphQLDirective({
      name: directiveName,
      locations: [DirectiveLocation.FIELD_DEFINITION],
      args: {
        scopes: { type: GraphQLString }
      }
    })
  }

  visitFieldDefinition(field: any) {
    const { resolve = defaultFieldResolver } = field

    const hasResolveFn = field.resolve !== undefined

    field.resolve = async (
      root: any,
      args: any,
      context: Context,
      info: any
    ) => {
      const auth = await authenticateUser(context)
      const mandatoryScopes = this.args.scopes

      const newContext = { ...context, auth }

      try {
        validateScopes(newContext, mandatoryScopes)
      } catch (error) {
        if (!hasResolveFn) {
          return null
        }

        throw error
      }

      return resolve.call(this, root, args, { ...newContext }, info)
    }
  }
}
