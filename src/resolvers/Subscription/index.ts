import Command from './Command'
import GameServer from './GameServer'

export const Subscription = {
  ...Command,
  ...GameServer
}
