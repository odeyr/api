/* eslint-disable @typescript-eslint/camelcase */
import { Context } from '../../utils'
import { MutationType, GameServerWhereInput } from '../../generated/prisma'

export default {
  gameServer: {
    subscribe: (
      _parent,
      args: {
        where: GameServerWhereInput
        mutationType: 'CREATED' | 'UPDATED' | 'DELETED' | MutationType[]
      },
      ctx: Context,
      info
    ) => {
      return ctx.db.subscription.gameServer(
        {
          where: {
            mutation_in: args.mutationType,
            node: args.where
          }
        },
        info
      )
    }
  }
}
