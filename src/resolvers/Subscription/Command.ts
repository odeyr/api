/* eslint-disable @typescript-eslint/camelcase */
import { Context } from '../../utils'
import { MutationType } from '../../generated/prisma'

export default {
  command: {
    subscribe: (
      _parent,
      args: {
        serverId: string
        mutationType: 'CREATED' | 'UPDATED' | 'DELETED' | MutationType[]
      },
      ctx: Context,
      info
    ) => {
      return ctx.db.subscription.command(
        {
          where: {
            node: {
              server: { id: args.serverId }
            },
            mutation_in: args.mutationType
          }
        },
        info
      )
    }
  }
}
