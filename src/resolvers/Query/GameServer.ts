import { forwardTo } from 'prisma-binding'

export default {
  gameServer: forwardTo('db'),
  gameServers: forwardTo('db')
}
