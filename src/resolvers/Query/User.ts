import { Context } from '../../utils'

export default {
  me(_parent, _args, ctx: Context, infos) {
    return ctx.db.query.user({ where: { id: ctx.auth.userId } }, infos)
  }
}
