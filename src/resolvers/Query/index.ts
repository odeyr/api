import User from './User'
import Command from './Command'
import GameServer from './GameServer'
import Game from './Game'
import Telemetry from './Telemetry'

export const Query = {
  ...User,
  ...Command,
  ...GameServer,
  ...Game,
  ...Telemetry
}
