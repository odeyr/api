import { Context } from '../../utils'
import { ApolloError } from 'apollo-server'

import { forwardTo } from 'prisma-binding'
import { AvailableGame, GameType } from '../../generated/prisma-client'

export default {
  game: forwardTo('db'),
  games: forwardTo('db'),
  async gamesNames(
    _parent,
    args: {},
    ctx: Context
    )   {
      
      var alldata = await ctx.db.query.games({})
      if (!alldata)
        throw new ApolloError('No games supported on the system')
      
      var filteredArray = alldata.reduce((acc, element) => {
        if (!acc.includes(element.name)) {
            acc.push(element.name)
        }
        return acc
      }, [])

      return filteredArray
  },
  async gamesTypes(
    _parent,
    args: { gameName: AvailableGame },
    ctx: Context
    )   {
      var alldata = await ctx.prisma.games({ where: { name: args.gameName }})
      if (!alldata)
        throw new ApolloError('No types supporting ' + args.gameName)
      var obj = {};

      var filteredArray = alldata.reduce((acc, element) => {
        if (!acc.includes(element.type)) {
            acc.push(element.type)
        }
        return acc
      }, [])
      
      return filteredArray
  },
  async gamesVersions(
    _parent,
    args: { gameName: AvailableGame, gameType: GameType },
    ctx: Context
    )   {
      var alldata = await ctx.prisma.games({ where: { name: args.gameName, type: args.gameType }})
      if (!alldata)
        throw new ApolloError('No versions supporting ' + args.gameName + ' and ' + args.gameType)
      
      var filteredArray = alldata.reduce((acc, element) => {
        if (!acc.includes(element.version)) {
            acc.push(element.version)
        }
        return acc
      }, [])
      
      return filteredArray
  }
}
