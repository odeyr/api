import { forwardTo } from 'prisma-binding'

export default {
  command: forwardTo('db'),
  commands: forwardTo('db')
}
