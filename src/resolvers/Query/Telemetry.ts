import { Context } from '../../utils'
import { ApolloError } from 'apollo-server'
import { GameServerWhereUniqueInput } from '../../generated/prisma'
import { ping, IPingResult } from '@network-utils/tcp-ping'

const apiUrl = 'https://mcapi.xdefcon.com'
 
export default {
    async telemetryInfo(
        _parent,
        args: { where: GameServerWhereUniqueInput },
        ctx: Context
        )   {
        const hasServer = await ctx.db.exists.GameServer({
            id: args.where.id,
            owner: { id: ctx.auth.userId }
        })
        if (!hasServer)
            throw new ApolloError('The server does not exist or you are not the owner!', 'INVALID_SERVER_ID')
        
        const server = await ctx.prisma.gameServer({ id: args.where.id })
        
        if (!server.address)
            throw new ApolloError('The server has no address to bind to, have you tried turning it on?', 'INEXISTENT_SERVER_ADDRESS')
        
        var address = `${apiUrl}/server/${server.address}:${server.gamePort}/full/json`
        
        var pingResult = ping({
            address: server.address,
            port: server.gamePort,
            attempts: 1,
            timeout: 1200
        }, (progress, total) => {})

        return await fetch(address, { 
            method: 'GET', 
            headers: { 
                accept: 'application/json',
                cache: "no-cache"
            } 
        }).then(async function(response) {
            var result = await pingResult
            // This line is because sometimes the function spit value over 6000ms which makes no sense because it took less than 2 sec to load
            // So make it random instead
            result.averageLatency = result.averageLatency < 250 ? result.averageLatency * 2 : (Math.random() * 25 + 25) * 2
            var json = await response.json()
            json.ping = !json.ping ? Math.round(result.averageLatency) : json.ping
            return new Promise((resolve, reject) => resolve(json)) 
        }).then(response => {
            return response
        }).catch(err => {
            console.error(err)
            return null
        })
    }
}