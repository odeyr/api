import { Query } from './Query'
import { Subscription } from './Subscription'
import { Mutation } from './Mutation'

export default {
  Query,
  Mutation,
  Subscription
}
