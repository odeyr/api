import Auth from './Auth'
import Command from './Command'
import GameServer from './GameServer'
import User from './User'

export const Mutation = {
  ...Auth,
  ...Command,
  ...GameServer,
  ...User
}
