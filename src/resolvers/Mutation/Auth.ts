import * as bcrypt from 'bcryptjs'
import {
  Context,
  signUser,
  getScopes,
  setTokensCookies,
  sendConfirmationEmail,
  FORGOT_PASSWORD_TOKEN_TIME
} from '../../utils'
import { ApolloError } from 'apollo-server'
import { addFragmentToInfo } from 'graphql-binding'
import {
  userWithScopesFragment,
  UserWithScopes,
  confirmationWithUserAndScopesFragment,
  ConfirmationTokenWithUserAndScopes
} from '../../fragments'
import * as ms from 'ms'
import * as token from 'crypto-token'
import mailService from '../../services/mailService'

export default {
  async signup(
    _parent,
    args: { email: string; password: string; username: string },
    ctx: Context
  ) {
    const password = await bcrypt.hash(args.password, 10)
    // TODO: Implement password stength

    let user: UserWithScopes

    try {
      user = (await ctx.prisma
        .createUser({
          ...args,
          password,
          confirmationToken: {
            create: {
              token: '',
              expiresAt: new Date()
            }
          }
        })
        .$fragment(userWithScopesFragment)) as UserWithScopes

      sendConfirmationEmail(user, ctx).then(user2 => {
        user = user2
      })
    } catch (e) {
      if (e.result) {
        e.result.errors.forEach(error => {
          if (error.code === 3010)
            throw new ApolloError('Email Already Used!', 'EMAIL_ALREADY_USED')
        })
      }

      throw e
    }

    // TODO: Call the login mutation instead
    const signedTokens = signUser(
      user.id,
      user.refreshTokenVersion,
      user.confirmationToken.confirmed,
      getScopes(user.permissions)
    )

    setTokensCookies(signedTokens, ctx)

    return user
  },

  async login(_parent, { email, password }, ctx: Context) {
    const user = (await ctx.prisma
      .user({ email })
      .$fragment(userWithScopesFragment)) as UserWithScopes
    if (!user)
      throw new ApolloError('Invalid credentials', 'CREDENTIALS_INVALID')

    const valid = await bcrypt.compare(password, user.password)
    if (!valid)
      throw new ApolloError('Invalid credentials', 'CREDENTIALS_INVALID')

    const signedTokens = signUser(
      user.id,
      user.refreshTokenVersion,
      user.confirmationToken.confirmed,
      getScopes(user.permissions)
    )

    setTokensCookies(signedTokens, ctx)

    return user
  },

  async logout(_parent, _args, ctx: Context) {
    ctx.response.cookie('odeyr-refresh-token', '', {
      maxAge: 0,
      httpOnly: true,
      secure: process.env.NODE_ENV === 'production' || false
    })
    ctx.response.cookie('odeyr-access-token', '', {
      maxAge: 0,
      httpOnly: true,
      secure: process.env.NODE_ENV === 'production' || false
    })

    return true
  },

  async resendConfirmationEmail(_parent, _args, ctx: Context) {
    const user = (await ctx.prisma
      .user({ id: ctx.auth.userId })
      .$fragment(userWithScopesFragment)) as UserWithScopes

    return await sendConfirmationEmail(user, ctx)
  },

  async sendForgotPasswordEmail(
    _parent,
    args: { email: string },
    ctx: Context
  ) {
    const userExist = await ctx.db.exists.User({
      email: args.email
    })

    if (!userExist)
      throw new ApolloError('The user does not exist!', 'USER_INVALID')

    await ctx.db.mutation.updateManyForgotPasswordTokens({
      data: { used: true },
      where: { user: { email: args.email } }
    })

    const forgotPasswordToken = token(32)

    const forgotPassword = await ctx.db.mutation.createForgotPasswordToken({
      data: {
        token: forgotPasswordToken,
        user: { connect: { email: args.email } },
        expiresAt: new Date(Date.now() + ms(FORGOT_PASSWORD_TOKEN_TIME))
      }
    })

    await mailService.send(
      args.email,
      ctx.request.__('email.forgot_password_subject'),
      'password_reset',
      ctx.request.locale,
      {
        recoveryLink: `${process.env.VERIFICATION_DOMAIN}/${ctx.request.locale}/password-reset/${forgotPasswordToken}`
      }
    )

    return forgotPassword
  },

  async resetPassword(
    _parent,
    args: { password: string; token: string },
    ctx: Context,
    infos
  ) {
    const tokenExpired = await ctx.db.exists.ForgotPasswordToken({
      token: args.token,
      // eslint-disable-next-line @typescript-eslint/camelcase
      expiresAt_gt: new Date(),
      used: false
    })

    if (!tokenExpired)
      throw new ApolloError('The token has expired!', 'TOKEN_EXPIRED')

    const password = await bcrypt.hash(args.password, 10)

    const token = await ctx.db.query.forgotPasswordToken(
      {
        where: { token: args.token }
      },
      '{ user { id } }'
    )

    return await ctx.db.mutation.updateUser(
      {
        data: {
          password: password,
          forgotPasswordTokens: {
            update: { data: { used: true }, where: { token: args.token } }
          }
        },
        where: { id: token.user.id }
      },
      infos
    )
  },

  async confirmAccount(_parent, args: { token: string }, ctx: Context, infos) {
    const tokenExist = await ctx.db.exists.EmailConfirmationToken({
      token: args.token,
      // eslint-disable-next-line @typescript-eslint/camelcase
      expiresAt_gt: new Date()
    })

    if (!tokenExist)
      throw new ApolloError('The token has expired!', 'TOKEN_EXPIRED')

    try {
      const confirmationToken = (await ctx.db.mutation.updateEmailConfirmationToken(
        {
          data: { confirmed: true },
          where: { token: args.token }
        },
        addFragmentToInfo(infos, confirmationWithUserAndScopesFragment)
      )) as ConfirmationTokenWithUserAndScopes

      const signedTokens = signUser(
        confirmationToken.user.id,
        confirmationToken.user.refreshTokenVersion,
        confirmationToken.user.confirmationToken.confirmed,
        getScopes(confirmationToken.user.permissions)
      )

      setTokensCookies(signedTokens, ctx)

      return confirmationToken
    } catch {
      throw new ApolloError('The token is invalid!', 'TOKEN_INVALID')
    }
  },

  async invalidateTokens(_parent, _args, ctx: Context) {
    const user = (await ctx.prisma
      .user({ id: ctx.auth.userId })
      .$fragment(userWithScopesFragment)) as UserWithScopes
    if (!user) return false

    await ctx.prisma.updateUser({
      data: { refreshTokenVersion: user.refreshTokenVersion + 1 },
      where: { id: ctx.auth.userId }
    })

    return true
  }
}
