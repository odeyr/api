import * as bcrypt from 'bcryptjs'
import { UserUpdateInput, UserWhereUniqueInput } from '../../generated/prisma'
import { Context, sendConfirmationEmail } from '../../utils'
import { UserWithScopes, userWithScopesFragment } from '../../fragments'
import { ApolloError } from 'apollo-server'
import { addFragmentToInfo } from 'graphql-binding'

const updateUser = async (
  _parent,
  args: { data: UserUpdateInput; where: UserWhereUniqueInput },
  ctx: Context,
  infos
) => {
  // Check user exist
  const userExist = await ctx.db.exists.User({
    id: args.where.id
  })

  if (!userExist)
    throw new ApolloError('The user does not exist!', 'INVALID_USER_ID')

  const updatedUser: UserUpdateInput = {}

  if (args.data.username) updatedUser.username = args.data.username

  // Hash the password
  if (args.data.password)
    updatedUser.password = await bcrypt.hash(args.data.password, 10)

  // Update the email
  if (args.data.email) {
    const emailExist = await ctx.db.exists.User({ email: args.data.email })
    if (emailExist)
      throw new ApolloError('Email Already Used!', 'EMAIL_ALREADY_USED')

    const user = (await ctx.db.query.user(
      { where: { id: args.where.id } },
      addFragmentToInfo(infos, userWithScopesFragment)
    )) as UserWithScopes

    user.email = args.data.email
    updatedUser.email = args.data.email

    try {
      await sendConfirmationEmail(user, ctx)
    } catch (e) {
      throw e
    }
  }

  // Forward to prisma
  return await ctx.db.mutation.updateUser(
    { data: updatedUser, where: args.where },
    infos
  )
}

const updateMe = async (
  _parent,
  args: { data: UserUpdateInput },
  ctx: Context,
  infos
) => {
  return await updateUser(
    _parent,
    { data: args.data, where: { id: ctx.auth.userId } },
    ctx,
    infos
  )
}

export default {
  updateUser,
  updateMe
}
