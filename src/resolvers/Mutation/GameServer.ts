import { Context } from '../../utils'
import {
  GameServerWhereUniqueInput,
  ServerPropertyCreateWithoutServerInput
} from '../../generated/prisma'
import { forwardTo } from 'prisma-binding'
import { sanitize } from 'string-sanitizer'
import { GameServerCreateInput, GameServerUpdateInput } from '../../schemaTypes'
import { ApolloError } from 'apollo-server'

export default {
  async createGameServer(
    _parent,
    args: { data: GameServerCreateInput },
    ctx: Context,
    infos
  ) {
    args.data.slug = sanitize.addDash(args.data.name).toLowerCase()
    args.data.owner = { connect: { id: ctx.auth.userId } }

    // Get all default properties
    const defaultServerProperties = await ctx.db.query.defaultServerProperties(
      {},
      '{ id, key, value }'
    )
    const serverProperties = defaultServerProperties.map(property => {
      return {
        key: property.key,
        value: property.value,
        defaultValue: { connect: { id: property.id } }
      }
    }) as ServerPropertyCreateWithoutServerInput[]
    args.data.properties = { create: serverProperties }

    // Connect the current user to the created guild
    if (args.data.guild && args.data.guild.create)
      args.data.guild.create.owner = { connect: { id: ctx.auth.userId } }

    const availablePorts = await ctx.db.query.availablePortses({ first: 1 })

    if (availablePorts.length > 0) {
      args.data.gamePort = availablePorts[0].gamePort
      args.data.mapPort = availablePorts[0].mapPort

      await ctx.db.mutation.deleteAvailablePorts({
        where: { id: availablePorts[0].id }
      })
    } else {
      let lastPorts = (await ctx.db.query.gameServers(
        { last: 1, orderBy: 'gamePort_ASC' },
        '{ gamePort, mapPort }'
      )) as { gamePort: number; mapPort: number }[]

      if (lastPorts.length === 0) {
        lastPorts = [
          {
            gamePort: 49150, // -2 because of the +2 at line 53
            mapPort: 49151 // -2 because of the +2 at line 54
          }
        ]
      }

      if (lastPorts[0].mapPort + 1 >= 65535)
        throw new ApolloError(
          'All ports are already used, contact administrator at contact@odeyr.org',
          'ALL_PORTS_USED'
        )

      args.data.gamePort = lastPorts[0].gamePort + 2
      args.data.mapPort = lastPorts[0].mapPort + 2
    }

    return forwardTo('db')(_parent, args, ctx, infos)
  },

  async updateGameServer(
    _parent,
    args: {
      data: GameServerUpdateInput
      where: GameServerWhereUniqueInput
    },
    ctx: Context,
    infos
  ) {
    const server = await ctx.db.exists.GameServer({
      id: args.where.id,
      owner: { id: ctx.auth.userId }
    })

    if (!server)
      throw new ApolloError(
        'You cannot update a server that is not yours',
        'UNAUTHORIZED_ACTION_ON_SERVER'
      )

    // Inject data
    if (args.data.name)
      args.data.slug = sanitize.addDash(args.data.name).toLowerCase()
    if (args.data.guild && args.data.guild.create)
      args.data.guild.create.owner = { connect: { id: ctx.auth.userId } }

    return forwardTo('db')(_parent, args, ctx, infos)
  },

  async deleteGameServer(
    _parent,
    args: { where: GameServerWhereUniqueInput },
    ctx: Context,
    infos
  ) {
    const server = await ctx.db.exists.GameServer({
      id: args.where.id,
      owner: { id: ctx.auth.userId }
    })

    if (!server)
      throw new ApolloError('The server does not exist!', 'INVALID_SERVER_ID')

    const usedPorts = (await ctx.db.query.gameServer(
      { where: args.where },
      '{ gamePort, mapPort }'
    )) as { gamePort: number; mapPort: number }

    await ctx.db.mutation.createAvailablePorts({ data: usedPorts })

    return forwardTo('db')(_parent, args, ctx, infos)
  }
}
