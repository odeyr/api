import { Context } from '../../utils'
import { ApolloError } from 'apollo-server'
import { CommandCreateInput, CommandUpdateInput } from '../../schemaTypes'
import { forwardTo } from 'prisma-binding'
import {
  GameServerWhereUniqueInput,
  CommandWhereUniqueInput
} from '../../generated/prisma'

export default {
  async createCommand(
    _parent,
    args: { data: CommandCreateInput; server: GameServerWhereUniqueInput },
    ctx: Context,
    infos
  ) {
    const server = await ctx.db.exists.GameServer({
      id: args.server.id,
      owner: { id: ctx.auth.userId }
    })

    if (!server)
      throw new ApolloError('The server does not exist!', 'INVALID_SERVER_ID')

    // Inject data
    args.data.server = { connect: { id: args.server.id } }
    args.data.issuer = { connect: { id: ctx.auth.userId } }

    return forwardTo('db')(_parent, args, ctx, infos)
  },

  async updateCommand(
    _parent,
    args: { data: CommandUpdateInput; where: CommandWhereUniqueInput },
    ctx: Context,
    infos
  ) {
    const command = await ctx.db.exists.Command({
      id: args.where.id,
      issuer: { id: ctx.auth.userId }
    })

    if (!command)
      throw new ApolloError('The command does not exist!', 'INVALID_COMMAND_ID')

    return forwardTo('db')(_parent, args, ctx, infos)
  },

  async deleteCommand(
    _parent,
    args: { where: CommandWhereUniqueInput },
    ctx: Context,
    infos
  ) {
    const command = await ctx.db.exists.Command({
      id: args.where.id,
      issuer: { id: ctx.auth.userId }
    })

    if (!command)
      throw new ApolloError('The command does not exist!', 'INVALID_COMMAND_ID')

    return forwardTo('db')(_parent, args, ctx, infos)
  },

  async clearCommands(
    _parent,
    args: { server: GameServerWhereUniqueInput },
    ctx: Context,
    infos
  ) {
    const command = await ctx.db.exists.Command({
      server: { id: args.server.id, owner: { id: ctx.auth.userId } }
    })

    if (!command)
      throw new ApolloError('The server does not exist!', 'INVALID_SERVER_ID')

    return ctx.db.mutation.deleteManyCommands(
      {
        where: {
          server: { id: args.server.id, owner: { id: ctx.auth.userId } }
        }
      },
      infos
    )
  }
}
