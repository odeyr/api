import * as mailgun from 'mailgun-js'

const mailgunClient = mailgun({
  apiKey: process.env.MAILGUN_API_KEY,
  domain: process.env.MAILGUN_DOMAIN
})

export default {
  send: async (
    to: string,
    subject: string,
    template: string,
    lang: string,
    variables: any
  ) => {
    try {
      if (process.env.REDIRECT_EMAIL_TO) to = process.env.REDIRECT_EMAIL_TO

      return await mailgunClient.messages().send({
        from: process.env.EMAIL_FROM,
        to,
        subject,
        template,
        't:version': lang,
        'o:tag': template,
        'h:X-Mailgun-Variables': JSON.stringify(variables)
      })
    } catch (err) {
      return Promise.reject(err)
    }
  }
}
