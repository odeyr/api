import {
  UserWhereUniqueInput,
  GameServerStatus,
  DiscordGuildWhereUniqueInput,
  GameServerWhereUniqueInput,
  ServerPropertyCreateManyWithoutServerInput
} from './generated/prisma'

// TODO: Add properties to every interface that needs it
// NOTE: Any properties marked as 'INJECTED' must be injected by the resolver

export interface CommandCreateInput {
  commandText: string
  result?: string
  server?: { connect: GameServerWhereUniqueInput } // INJECTED
  issuer?: { connect: UserWhereUniqueInput } // INJECTED
}

export interface CommandUpdateInput {
  commandText?: string
  result?: string
}

export interface GameServerCreateInput {
  name: string
  slug?: string // INJECTED
  owner?: { connect: UserWhereUniqueInput } // INJECTED
  guild?: DiscordGuildCreateOneStrippedInput // If creating, INJECTED
  gamePort?: number // INJECTED
  mapPort?: number // INJECTED
  properties?: ServerPropertyCreateManyWithoutServerInput // INJECTED
}

export interface GameServerUpdateInput {
  name?: string
  slug?: string // INJECTED
  status?: GameServerStatus
  address?: string
  guild?: DiscordGuildUpdateOneStrippedInput // If creating, INJECTED
}

export interface GameServerConnectOneStrippedInput {
  connect: GameServerWhereUniqueInput
}

export interface DiscordGuildCreateOneStrippedInput {
  create?: DiscordGuildCreateInput
  connect?: DiscordGuildWhereUniqueInput
}

export interface DiscordGuildUpdateOneStrippedInput {
  create?: DiscordGuildCreateInput
  connect?: DiscordGuildWhereUniqueInput
  disconnect?: boolean
  delete?: boolean
  update?: DiscordGuildUpdateInput
}

export interface DiscordGuildCreateInput {
  guildId: string
  guildName: string
  channelId: string
  channelName: string
  owner?: { connect: UserWhereUniqueInput } // To be injected
}

export interface DiscordGuildUpdateInput {
  guildId?: string
  guildName?: string
  channelId?: string
  channelName?: string
  owner?: { connect: UserWhereUniqueInput } // To be injected
}
