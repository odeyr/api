# Odeyr API

Ceci est l'API de la plateforme Odeyr. C'est le coeur de la plateforme. L'API permet de faire le lien entre tous les composants et de gérer la base de données.

L'API est conçu en [GraphQL](https://graphql.org) et [TypeScript](https://www.typescriptlang.org/). Sous le capot, l'API utilise aussi [Prisma](https://prisma.io) pour la communication avec la base de données.

## Étapes d'installation pour développement

### Installation des logiciels nécessaires

1. Installer [docker](https://www.docker.com/) avec [docker-compose](https://docs.docker.com/compose/)
2. Installer [Node.js](https://nodejs.org) et npm
3. Installer [Git](https://git-scm.com/)
4. Cloner le repo
5. (Recommandé) Sélectionner la branche `develop`

### Installation de la base de données

1. Exécuter la commande: `export MANAGEMENT_API_SECRET="my-server-secret-123"`
2. Exécuter la commande: `export DATABASE_PASSWORD="prisma"`
3. Exécuter la commande: `docker-compose -f docker-compose.prisma.yml up -d`

Si tout c'est bien déroulé, en exécutant la commande `docker ps` vous devriez voir ceci:

![Expected docker ps](https://cdn.odeyr.org/readme/expected_docker_ps_odeyr.png "docker ps result")

### Configuration du serveur GraphQL

1. Dupliquer le fichier `development.exemple.env`
2. Renommer le fichier dupliqué à `.env` (Aucun nom, juste l'extension)
3. Modifier le contenu du fichier `.env` pour l'ajuster à votre système

#### À propos des courriels

L'API utilise Mailgun pour envoyer des courriels transactionnels. Il est nécessaire d'obtenir une clé d'API et un domaine d'envoi valide. Mailgun offre un domaine dit "sandbox" pour les tests. Veuillez vous référer à la documentation de Mailgun pour l'utilisation du domaine d'envoi.

Afin d'éviter d'envoyer des courriels dans le vide ou à des personnes qui ne souhaiteraient pas en recevoir, il est aussi recommandé de modifier la variable d'environnement `REDIRECT_EMAIL_TO` afin que tous les courriels transactionnels soient automatiquement redirigés vers votre adresse courriel sans égard au courriel de l'utilisateur.

### Installation du serveur GraphQL
Il est conseillé de rouler les prochaines commandes dans git bash. Si vous êtes dans powershell, quelques directives suplémentaires pourraient s'appliquer.
1. Exécuter la commande: `npm i -g prisma graphql-cli`
2. Exécuter la commande: `npm i`
3. Exécuter la commande: `prisma deploy`
    1. Si la commande retourne une erreur 'is not numerically signed', ouvrir powershell en mode administrateur et exécuter `Set-ExecutionPolicy Unrestricted`, puis réessayer le point 3
4. Exécuter la commande: `prisma generate`
5. (Optionnel) Exécuter la commande: `prisma seed`

Si tout s'est bien déroulé, vous devriez être en mesure de lancer le serveur GraphQL avec la commande: `npm run dev`

#### À propos de la connexion à l'API

En accédant au serveur web lancé par l'API, vous devriez être en mesure de commencer à exécuter des requêtes manuellement. La plupart des ressources exposées par l'API requièrent d'être authentifiées. L'API utilise des cookies d'authentification et tente de respecter le principe de *Refresh Token* et *Access Token*. L'environnement graphique de test ne supporte pas les cookies par défaut. Il faut aller dans les paramètres et modifier la ligne: `"request.credentials": "omit"` à `"request.credentials": "include"`
