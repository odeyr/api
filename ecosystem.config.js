/* eslint-disable @typescript-eslint/camelcase */
// Target server hostname or IP address
const TARGET_SERVER_HOST = process.env.TARGET_SERVER_HOST
  ? process.env.TARGET_SERVER_HOST.trim()
  : ''
// Target server username
const TARGET_SERVER_USER = process.env.TARGET_SERVER_USER
  ? process.env.TARGET_SERVER_USER.trim()
  : ''
const BRANCH = process.env.CI_COMMIT_REF_SLUG
  ? process.env.CI_COMMIT_REF_SLUG
  : ''
// Target server application path
const TARGET_SERVER_APP_PATH = `/home/${TARGET_SERVER_USER}/api.odeyr.org/${BRANCH}`
// Your repository
const REPO = 'git@gitlab.com:odeyr/api.git'

module.exports = {
  /**
   * Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   */
  apps: [
    {
      name: 'odeyr-api',
      script: 'npm',
      args: 'start',
      append_env_to_name: true,
      env: {
        NODE_ENV: 'development'
      },
      env_development: {
        NODE_ENV: 'development'
      },
      env_production: {
        NODE_ENV: 'production'
      }
    }
  ],

  /**
   * Deployment section
   * http://pm2.keymetrics.io/docs/usage/deployment/
   */
  deploy: {
    production: {
      user: TARGET_SERVER_USER,
      host: TARGET_SERVER_HOST,
      ref: 'origin/master',
      repo: REPO,
      path: TARGET_SERVER_APP_PATH,
      'post-deploy':
        'cp ../.env .env' +
        ' && npm install' +
        ' && prisma deploy --force' +
        ' && prisma generate' +
        ' && pm2 startOrRestart ecosystem.config.js --env=development' +
        ' && pm2 save'
    },
    development: {
      user: TARGET_SERVER_USER,
      host: TARGET_SERVER_HOST,
      ref: 'origin/develop',
      repo: REPO,
      path: TARGET_SERVER_APP_PATH,
      'post-deploy':
        'cp ../.env .env' +
        ' && npm install' +
        ' && prisma deploy --force' +
        ' && prisma generate' +
        ' && pm2 startOrRestart ecosystem.config.js --env=development' +
        ' && pm2 save'
    }
  }
}
