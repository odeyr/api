FROM node:12.16.0-alpine3.10

# Create app directory
WORKDIR /app

# Install app dependencies
RUN npm config set unsafe-perm true
# A wildcard is used to ensure both package.json AND package-lock.json are copied
COPY package*.json ./

RUN npm install

# Bundle app source
COPY . /app

# serve on port 4000
EXPOSE 4000
CMD ["npm", "start"]